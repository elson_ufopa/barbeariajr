# BarbeariaJR

Instruções de instalação no windows

Resquisitos de sofware instalados no sistema windows:
* php 8.0 [Baixar](https://www.php.net/downloads.php)
* VScode [Baixar](https://code.visualstudio.com/)
* WorkBench [Baixar](https://dev.mysql.com/downloads/workbench/)
* GitHub [Instalar](https://github.com/git-guides/install-git)

1- git clone https://gitlab.com/elson_ufopa/barbeariajr.git

2- Instalar o gerenciador de dependências composer:

* Composer [Baixar](https://getcomposer.org/download/)

3- Entrar na pasta /back_end e executar o seguinte comando
```
composer update
```
4- Iniciar o programa WorkBench e criar um banco de dados:

backend

5- Executar o comando abaixo para criar as tabelas: 
```
php artisan migrate:fresh

```
6- Iniciar o servidor back-end (port: 3333)
```
php artisan serve
```
7- Utilizar o Insomnia para testar as rotas ou outro de sua preferencia, rotas

   disponiveis para test:

   http://127.0.0.1:8000/api/user 

   http://127.0.0.1:8000/api/auth/login

   exemplos de chamadas:

   Criacao de usuario:

   ```
    {
	"name": "barbeiro",
	"email": "barbeiro@gmail.com",
	"password": "123456"
}
   ```
   Autenticacao de usuario:
   ```
   {
	"email": "barbeiro@gmail.com",
	"password": "123456"
   }
   ```


